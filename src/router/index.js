import { createWebHistory, createRouter } from 'vue-router'
import Home from '../pages/Home.vue'
import About from '../pages/About.vue'
import Article from '../pages/Article.vue'

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/article/:articleHash',
    name: 'Article',
    component: Article
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
